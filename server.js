const express = require("express"),
bodyParser = require("body-parser"),
swaggerJsdoc = require("swagger-jsdoc"),
swaggerUi = require("swagger-ui-express");
const rootRouter = require("./app/routes/");

const cors = require("cors");
const app = express();
var corsOptions = {
  origin: "http://localhost:3000"
};


const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Rest Api Docs",
      version: "0.1.0",
      description:
        "This is a simple CRUD API application made with Express and documented with Swagger",
      contact: {
        name: "Rightway",
      },
    },
    components: {
      securitySchemas: {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        }
      }
    },
    security: [{
      bearerAuth: [],
     }
    ],
    servers: [
      {
        url: "http://localhost:3000/tutorials",
      },
    ],
  },
  //apis: [".app/routes/index.js", ".app/models/index.js"],
  apis: [".app/routes/index.js", ".app/models/index.js"],
};

const specs = swaggerJsdoc(options);
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);

// function swaggerDocs(app: Express, port: number){


  
// }




const db = require("./app/models");
db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
});

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
//   });

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// simple route
// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to rightway application." });
// });


// API routes
app.use("", rootRouter);

// Routes

/**
 * @swagger
 * /tutorials
 * get:
 *    description: Use to request all customers
 *    responses:
 *       '200':
 *         description: a successfull response
 */
// app.get('/tutorials', (req, res)=>{
//   res.status(200).send('customer serve');
// })

//require("./app/routes/tutorial.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});