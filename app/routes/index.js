const express = require('express');
const tutorialRouter = require('./tutorial.routes');
const bookRouter = require('./book.routes');

const rootRouter = express.Router();
rootRouter.use("/api/books", bookRouter);
rootRouter.use("/", tutorialRouter);

module.exports = rootRouter;