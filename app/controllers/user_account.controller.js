const db = require("../models");
console.log('db', db)
const UserAcount = db.UserAcount;

exports.create = (req, res) => {
    // Validate request
    if (!req.body.first_name) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
    // Create a Tutorial
    const users = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      gender_id: req.body.gender_id,
      nickname: req.body.nickname,
      email: req.body.email,
      popularity: req.body.popularity,
      confirmation_code: req.body.confirmation_code,
      deatils: req.body.deatils
//      published: req.body.published ? req.body.published : false
    };
    // Save Tutorial in the database
    UserAcount.create(users)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial."
        });
      });
  };
