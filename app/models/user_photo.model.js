module.exports = (sequelize, Sequelize) => {
    const UserPhoto = sequelize.define("user_photo", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_account_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'user_accounts',
              key: 'id',
            }
        }, 
        link: {
            type: Sequelize.TEXT,
            allowNull: true, 
            defaultValue: null
        },
        details: {
            type: Sequelize.TEXT,
            allowNull: true, 
            defaultValue: null
        },
        time_added: {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        active: {
            type: Sequelize.BOOLEAN,
            allowNull: true, 
            defaultValue: null
        },
    });
    return UserPhoto;
  };