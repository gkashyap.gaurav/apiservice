module.exports = (sequelize, Sequelize) => {
    const gender = sequelize.define("gender", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_account_id_given: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },  
      user_account_id_received: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },
      grade: {
        type: Sequelize.INTEGER,
        allowNull: true, 
        defaultValue: null
      },  
    });
    return gender;
  };