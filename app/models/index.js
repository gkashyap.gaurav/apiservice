const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.UserAcount = require("./user_account.model.js")(sequelize, Sequelize);
db.tutorials = require("./tutorial.model.js")(sequelize, Sequelize);

db.genders = require("./gender.model.js")(sequelize, Sequelize);

db.relationship_type = require("./relationship_type.model.js")(sequelize, Sequelize);
db.interested_in_relation = require("./interested_in_relation.model.js")(sequelize, Sequelize);
db.interested_in_gender = require("./interested_in_gender.model.js")(sequelize, Sequelize);
db.user_photo = require("./user_photo.model.js")(sequelize, Sequelize);
db.grade = require("./grade.model.js")(sequelize, Sequelize);
db.block_user = require("./block_user.model.js")(sequelize, Sequelize);

module.exports = db;