module.exports = (sequelize, Sequelize) => {
    const interstedInRelation = sequelize.define("interested_in_relation", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_account_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },  
      relationship_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          // This is a reference to another model
          model: 'relationship_types',
          // This is the column name of the referenced model
          key: 'id',
        }
      }     
    });
    return interstedInRelation;
  };