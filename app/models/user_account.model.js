module.exports = (sequelize, Sequelize) => {
    const UserAcount = sequelize.define("user_account", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: true, 
            defaultValue: null
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull: true, 
            defaultValue: null
        },
        gender_id: {
            type: Sequelize.STRING, 
            defaultValue: null
        },
        nickname: {
            type: Sequelize.STRING,
            allowNull: true, 
            defaultValue: null
        },
        email: {
            type: Sequelize.STRING, 
            defaultValue: null
        },
        popularity: {
            type: Sequelize.FLOAT,
            allowNull: true, 
            defaultValue: null
        },
        confirmation_code: {
            type: Sequelize.TEXT,
            allowNull: true, 
            defaultValue: null
        },
        deatils: {
            type: Sequelize.TEXT, 
            defaultValue: null
        },
        confirmation_time: {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },

    });
    return UserAcount;
  };