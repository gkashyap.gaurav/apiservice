module.exports = (sequelize, Sequelize) => {
    const interstedInGender = sequelize.define("interested_in_gender", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_account_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },  
      gender_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          // This is a reference to another model
          model: 'genders',
          // This is the column name of the referenced model
          key: 'id',
        }
      }     
    });
    return interstedInGender;
  };