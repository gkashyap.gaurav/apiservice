module.exports = (sequelize, Sequelize) => {
    const RelationshipType = sequelize.define("relationship_type", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true, 
        defaultValue: null
      },
     
    });
    return RelationshipType;
  };