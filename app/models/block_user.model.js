module.exports = (sequelize, Sequelize) => {
    const block_user = sequelize.define("block_user", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_account_id_given: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },  
      user_account_id_blocked: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_accounts',
          key: 'id',
        }
      },
       
    });
    return block_user;
  };